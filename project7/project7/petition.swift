//
//  petition.swift
//  project7
//
//  Created by Izundu Ngwu on 3/23/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import Foundation

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}
