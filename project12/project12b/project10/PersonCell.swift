//
//  PersonCell.swift
//  project10
//
//  Created by Izundu Ngwu on 3/28/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
}
