//
//  Person.swift
//  project10
//
//  Created by Izundu Ngwu on 3/28/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit

class Person: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(imageUUID, forKey: "imageUUID")
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        imageUUID = aDecoder.decodeObject(forKey: "imageUUID") as? String ?? ""
    }
    
    var name: String
    var imageUUID: String

    init(name: String, imageUUID: String){
        self.name = name
        self.imageUUID = imageUUID
    }
}
