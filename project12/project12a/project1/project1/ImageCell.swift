//
//  ImageCell.swift
//  project1
//
//  Created by Izundu Ngwu on 3/29/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var imageName: UILabel!    
}
