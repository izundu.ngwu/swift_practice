//
//  WikipediaViewController.swift
//  project16
//
//  Created by Izundu Ngwu on 4/4/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit
import WebKit

class WikipediaViewController: UIViewController, WKNavigationDelegate {
    var webView: WKWebView!
    var annotationWiki: String?
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: "https://en.wikipedia.org/wiki/" + annotationWiki!) {
            webView.load(URLRequest(url: url))
        }
        webView.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
