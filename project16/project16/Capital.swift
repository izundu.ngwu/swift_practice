//
//  Capital.swift
//  project16
//
//  Created by Izundu Ngwu on 4/3/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit
import MapKit

class Capital: NSObject, MKAnnotation {
    var title: String?
    var info: String
    var coordinate: CLLocationCoordinate2D
    var wikipage: String
    
    init(title: String, coordinate: CLLocationCoordinate2D, info: String, wikipage: String) {
        self.title = title
        self.info = info
        self.coordinate = coordinate
        self.wikipage = wikipage
    }

}
