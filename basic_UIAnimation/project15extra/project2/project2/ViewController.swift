//
//  ViewController.swift
//  project2
//
//  Created by Izundu Ngwu on 3/12/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    
    var countries = [String]()
    var questions = 0
    var score = 0
    var correctAnswer = 0
 
    func askQuestion(action: UIAlertAction! = nil) {
        // Shuffle countries
        countries.shuffle()
        // Create buttons with picture
        button1.setImage(UIImage(named: countries[0]), for: .normal)
        button2.setImage(UIImage(named: countries[1]), for: .normal)
        button3.setImage(UIImage(named: countries[2]), for: .normal)
        // randomize correct answer
        correctAnswer = Int.random(in: 0...2)
        title = countries[correctAnswer].uppercased() + " | Score: \(score)"
        // count questions
        questions += 1
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options: [], animations: {
            sender.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }) { (finished) in
            sender.transform = .identity
        }
        
        var title: String
        // check alert and increase score
        if sender.tag == correctAnswer {
            title = "Correct"
            score += 1
        } else {
            title = "Wrong, that's the flag of \(countries[sender.tag].uppercased())"
            score -= 1
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            //
            if self.questions == 3 {
                let ac = UIAlertController(title: "Complete!", message: "You answered 3. Your score is \(self.score).", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: self.askQuestion))
                self.present(ac, animated: true)
            } else if self.questions < 3 {
                let ac = UIAlertController(title: title, message: "Your score is \(self.score).", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: self.askQuestion))
                self.present(ac, animated: true)
            }
        }
    }
    @objc func buttonBounce() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "uk", "us"]
        // Set button features
        button1.layer.borderWidth = 1
        button2.layer.borderWidth = 1
        button3.layer.borderWidth = 1
        button1.layer.borderColor = UIColor.lightGray.cgColor
        button2.layer.borderColor = UIColor.lightGray.cgColor
        button3.layer.borderColor = UIColor.lightGray.cgColor
        // Run first instance to ask question
        askQuestion()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Score", style: .plain, target: self, action: #selector(showScore))
    }
    @objc func showScore() {
        let ac = UIAlertController(title: "Score", message: "Your score is \(score).", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: askQuestion))
        present(ac, animated: true)
    }
}

