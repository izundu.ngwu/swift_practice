//
//  SiteListViewController.swift
//  project4
//
//  Created by Izundu Ngwu on 3/15/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit

class SiteListViewController: UITableViewController {
    var websites = ["apple.com", "google.com", "twitter.com"]

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Websites"
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return websites.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "webpage", for: indexPath)
        cell.textLabel?.text = websites[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let wp = storyboard?.instantiateViewController(withIdentifier: "webView") as? ViewController {
            wp.selectedWebsite = websites[indexPath.row]
            navigationController?.pushViewController(wp, animated: true)
        }
    }
}
