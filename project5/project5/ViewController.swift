//
//  ViewController.swift
//  project5
//
//  Created by Izundu Ngwu on 3/16/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    var allWords = [String]()
    var usedWords = [String]()
    enum Errors {
        case impossible, unoriginal, incorrect, identical
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                allWords = startWords.components(separatedBy: "\n")
            }
        }
        if allWords.isEmpty {
            allWords = ["silkworm"]
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(promptForAnswer))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(startGame))
        startGame()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usedWords.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Word", for: indexPath)
        cell.textLabel?.text = usedWords[indexPath.row]
        return cell
    }
  //  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
   // }
    @objc func startGame() {
        title = allWords.randomElement()
        usedWords.removeAll(keepingCapacity: true)
        tableView.reloadData()
    }
    
    @objc func promptForAnswer() {
        let ac = UIAlertController(title: "Enter answer", message: nil, preferredStyle: .alert)
        ac.addTextField()
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) {
            [weak self, weak ac] action in
            guard let answer = ac?.textFields?[0].text else {return}
            self?.submit(answer)
        }
        ac.addAction(submitAction)
        present(ac, animated: true)
    }
    func submit(_ answer: String) {
        let lowerAnswer = answer.lowercased()
        
        if isIdentical(word: lowerAnswer) {
            if isPossible(word: lowerAnswer) {
                if isOriginal(word: lowerAnswer) {
                    if isReal(word: lowerAnswer) {
                        
                        usedWords.insert(lowerAnswer, at: 0)
                        let indexPath  = IndexPath(row: 0, section: 0)
                        tableView.insertRows(at: [indexPath], with: .automatic)
                        return
                        
                    } else {
                        showErrorMessage(error: .incorrect)
                    }
                } else {
                    showErrorMessage(error: .unoriginal)
                }
            } else {
                showErrorMessage(error: .impossible)
            }
        } else {
            showErrorMessage(error: .identical)
        }
    }
    
    func isPossible(word: String) -> Bool {
        guard var tempWord = title?.lowercased() else { return false }

        for letter in word {
            if let position = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: position)
            } else {
                return false
            }
        }
        return true
    }
    
    func isOriginal(word: String) -> Bool {
        return !usedWords.contains(word)
    }

    func isIdentical(word: String) -> Bool {
        guard let tempWord = title?.lowercased() else { return false }
        if word == tempWord {
            return false
        }
        return true
    }
    
    func isReal(word: String) -> Bool {
        if word.count < 3 { return false }
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        return misspelledRange.location == NSNotFound
    }
    
    func showErrorMessage(error: Errors) {
        let errorTitle : String
        let errorMessage: String
        
        switch error {
        case .impossible:
            guard let title = title?.lowercased() else {return}
            errorTitle = "Word not possible"
            errorMessage = "You can't spell that word from \(title)"
        case .identical:
            errorTitle = "Identical to starter word"
            errorMessage = "Please make a new word from original"
        case .incorrect:
            errorTitle = "Word not recognised"
            errorMessage = "You can't just make up words, you know!"
        case .unoriginal:
            errorTitle = "Word used already"
            errorMessage = "Be more original!"
        }
        let ac = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present (ac, animated: true)
    }
}

