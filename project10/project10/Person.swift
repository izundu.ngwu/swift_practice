//
//  Person.swift
//  project10
//
//  Created by Izundu Ngwu on 3/28/19.
//  Copyright © 2019 Izundu Ngwu. All rights reserved.
//

import UIKit

class Person: NSObject {
    var name: String
    var imageUUID: String

    init(name: String, imageUUID: String){
        self.name = name
        self.imageUUID = imageUUID
    }
}
